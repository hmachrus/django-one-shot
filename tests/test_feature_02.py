from django.test import SimpleTestCase


class TestDjangoProjectAndApp(SimpleTestCase):
    def test_brain_two_project_created(self):
        try:
            from brain_two.settings import INSTALLED_APPS  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find the Django project 'brain_two'")

    def test_todo_app_created(self):
        try:
            from todo.apps import todoConfig  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find the Django app 'todo'")

    def test_todo_app_installed(self):
        try:
            from brain_two.settings import INSTALLED_APPS

            self.assertIn("todo.apps.todoConfig", INSTALLED_APPS)
        except ModuleNotFoundError:
            self.fail("Could not find 'todo' installed in 'brain_two'")
